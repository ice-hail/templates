/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

// container
#include "./classes/rootLevelContainer.c"
#include "./classes/physicsData.c"
#include "./classes/physicsSystem.c"
#include "./classes/rigidBody.c"
#include "./classes/staticCompoundShape.c"
#include "./classes/listShape.c"

// shapes
#include "./classes/boxShape.c"
#include "./classes/sphereShape.c"
#include "./classes/convexVerticesShape.c"
#include "./classes/compressedMesh.c"

// transform
#include "./classes/convexTranslateShape.c"
#include "./classes/convexTransformShape.c"
