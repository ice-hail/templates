/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

int countClassNames() {
    local uint i = 0;
    local uint offset = FTell();
    local int nameLength = 0;
    while(ReadUByte(offset) != 0xFF && ReadUByte(offset) != 0x00) { 
        offset += 5;
        nameLength = ReadStringLength(offset);
        offset += nameLength;
        ++i;
    }
    return i;
}

void alignPointer(uint alignment) {
    if(FTell() % alignment != 0)
    {
        FSkip(alignment - (FTell() % alignment));
    }
}
 