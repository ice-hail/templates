struct StaticCompoundShapeSetting
{
    ushort setting0 <bgcolor=0xFF9900>; // not sure about the size
    ushort setting1 <bgcolor=0xFFBB00>; // not sure about the size
    ushort actorIdMaybe <bgcolor=0xFFFF00>; // not sure about the size

    Printf("Actor %X => %X %X\n", actorIdMaybe, setting0, setting1);
};

struct StaticCompoundShape
{
    int unknown0[8] <bgcolor=0x444477>;
    Counter entryCounter <bgcolor=0x9999FF>;
    Counter unknownCounter;
    alignPointer(16);
    Counter counter2 <bgcolor=0x9999FF>; // always(?) set to:  (entryCounter * 2) - 1
    alignPointer(16);

    Vector4 aabbMinMaybe <bgcolor=0x9944FF>;
    Vector4 aabbMaxMaybe <bgcolor=0x9966FF>;

    struct 
    {
        Vector4 pos <bgcolor=0x333333>;
        Vector4 rotation;
        Vector4 scale;
        FSkip(8);
        uint flagsMaybe;
        uint actorId <bgcolor=0x333377>;
    } entries[entryCounter.count] <optimize=false>;

    Printf("\n");
    StaticCompoundShapeSetting settings[counter2.count] <optimize=false>;
};