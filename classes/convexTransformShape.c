/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

struct ConvexTransformShape 
{
    int padding00[2];
    
    byte type;
    byte shapeDispatchType;
    byte bitsPerKey;
    byte codecInfo;

    int someInt;

    Vector4 radius <name="Radius??", bgcolor=0xEE5555>;

    Vector4 position <bgcolor=0xEE5555>;
    Vector4 rotation <bgcolor=0xEE5555>;
    Vector4 scale <bgcolor=0xEE5555>;
};
