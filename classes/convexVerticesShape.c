/**
* @copyright 2019 - Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

struct ConvexVerticesShape {
    uint basicShapeHeader[4] <bgcolor=0x333333>;
    Vector4 basicShapeRadius <bgcolor=0x333333>;

    Vector4 aabbsizeHalf <bgcolor=0xFF4466>;
    Vector4 aabbCenter <bgcolor=0xFF4466>;

    Counter vertexCount <bgcolor=0x44FF66>;
    FSkip(8);
    Counter planeCount <bgcolor=0x44FF66>;
    FSkip(16);
    
    TransposedMatrix3x4 vertices[vertexCount.count / 3] <bgcolor=0xFF6644>;
    Plane planes[planeCount.count] <bgcolor=0x4466FF>;
};